﻿using System.Collections;
using UnityEngine;

public class AssignComponentUtility : MonoBehaviour
{
    [SerializeField] private FatherComponent _childComponent;
    [SerializeField] private ExtendedChildrenList _extendedChildrenList;

    void Start()
    {
        StartCoroutine(DelayAssign());
    }

    private IEnumerator DelayAssign()
    {
        yield return new WaitForSeconds(.2f);

        //Set Reference
        // _extendedChildrenList.fatherComponents.Add(_childComponent);
        // _childComponent.transform.parent = _extendedChildrenList.transform;


        //SetNewComponent 
        //CopyComponent --> Only valid in the editor, not once the game is compiled
        // ComponentUtility.CopyComponent(_childComponent);
        // ComponentUtility.PasteComponentAsNew(_extendedChildrenList.gameObject);

        //ReloadList
        // _extendedChildrenList.fatherComponents.Clear();
        // _extendedChildrenList.fatherComponents.AddRange(_extendedChildrenList.GetComponents<FatherComponent>());

        //Instanciate -> The reference is still passed to it, although in this case a clone of the initial GameObject.
        FatherComponent newChildComponent = Instantiate(_childComponent, _extendedChildrenList.transform);
        _extendedChildrenList.fatherComponents.Add(newChildComponent);
        
        //Destoy
        Destroy(gameObject);
    }
}