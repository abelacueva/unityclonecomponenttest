﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

[Serializable] 
public class ExtendedChildrenList : MonoBehaviour
{
    public List<FatherComponent> fatherComponents = new List<FatherComponent>();
    public Text text;

    private void Start()
    {
        fatherComponents.AddRange(GetComponents<FatherComponent>());
       StartCoroutine(PlayDebug());
    }

    private IEnumerator PlayDebug()
    {
        while (true)
        {
            text.text = "Components: " + fatherComponents.Count; 
            yield return new WaitForSeconds(2f);
            text.text = ""; 
            fatherComponents.ForEach(component => text.text += component.PrintDebugValue()+"\n");
            yield return new WaitForSeconds(2f);
        }
    }
    
    
}
