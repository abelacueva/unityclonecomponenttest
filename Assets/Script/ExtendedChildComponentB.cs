﻿using UnityEngine;

public class ExtendedChildComponentB : FatherComponent
{
    [SerializeField]
    private string specialValueB;
    
    public override string PrintDebugValue()
    {
        Debug.Log("GO: " + name + " specialValueB: " +specialValueB);
        return "GO: " + name + " specialValueB: " + specialValueB;
    }
    
}
