﻿using System;
using UnityEngine;

public class ExtendedChildComponentA : FatherComponent
{
    
    [SerializeField]
    private string specialValueA;
    
    public override string PrintDebugValue()
    {
        base.PrintDebugValue();
        Debug.Log("GO: " + name + " specialValueA: " +specialValueA);
        return "GO: " + name + " specialValueA: " + specialValueA;
    }
    
}
