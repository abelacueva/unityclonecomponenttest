﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class FatherComponent : MonoBehaviour
{
    [SerializeField] private String fatherValue;

    public virtual string PrintDebugValue()
    {
        Debug.Log("GO: " + name + " fatherValue: " +fatherValue);
        return "GO: " + name + " fatherValue: " + fatherValue;
    }

}
